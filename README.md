# Sublime text .swp creator plugin #

This is repo with very simple Sublime Text plugin for automatic creating and removing vim-like .swp files.

### Caution ###

This plugin can **only** create and remove **empty** hidden .swp files. Plugin doesn't contains any kind of protection overwriting files.

### Why I need this? ###

You probably not, but I need this plugin because I am working on some project using Zope & Plone (Python framework) and if I want to edit source code in external editors (via zopeedit in this case) I need some functions for locking files creating a new lock file near the original one.

### Instalation how-to ###

I am using Sublime Text 3 (build 3059 now) and CrunchBang Linux (Debian-based), so I tested this plugin only with this enviroment.

* Download .py file to some place on your disk.
* Copy file to /home/<yourUserName>/.config/sublime-text-3/Packages/User folder.
* That's all. Simply check if .swp file is created when you open some file to edit.

### Contact me ###

* If you want version for Sublime Text 2, Windows/Mac or have some other feature requests
* If you find some bugs
* If you want to port this plugin to Sublime Package repository (I am not sure that I have enough time to do this)
