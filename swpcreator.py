import sublime, sublime_plugin, os

class SWPCreator(sublime_plugin.EventListener):
    # Opening existing file
    def on_load(self, view):
        # Get dir and file name from view
        dir_name = os.path.dirname(view.file_name())
        file_name = os.path.basename(view.file_name())
        # Create vim-like swp file name
        swp_file_name = '.' + file_name + '.swp'
        # Create empty file in same folder as opened file
        try:
            open(os.path.join(dir_name, swp_file_name), 'a').close()
        except Exception as e:
            # If we can't create swp file, just notify to console
            print('Can\'t create .swp file', e)

    # Save new file - same work as before
    def on_post_save(self, view):
        # Get dir and file name from view
        dir_name = os.path.dirname(view.file_name())
        file_name = os.path.basename(view.file_name())
        # Create vim-like swp file name
        swp_file_name = '.' + file_name + '.swp'
        # Create empty file in same folder as opened file
        try:
            open(os.path.join(dir_name, swp_file_name), 'a').close()
        except Exception as e:
            # If we can't create swp file, just notify to console
            print('Can\'t create .swp file', e)

    def on_close(self, view):
        try:
            # Get dir and file name from view
            dir_name = os.path.dirname(view.file_name())
            file_name = os.path.basename(view.file_name())
            # Create vim-like swp file name
            swp_file_name = '.' + file_name + '.swp'
            # Remove swp file
            os.remove(os.path.join(dir_name, swp_file_name))
        except Exception as e:
            # If we can't remove swp file or closing unsaved file, just notify
            print('Can\'t remove .swp file', e)
